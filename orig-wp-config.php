<?php
define('WP_HOME','http://77.104.152.211/~adamsaid/tharrisconsultants.com'); 
define('WP_SITEURL','http://77.104.152.211/~adamsaid/tharrisconsultants.com');
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if ($_SERVER['SERVER_NAME'] === "localhost.com") {
   // ** MySQL settings - You can get this info from your web host ** //
  /** The name of the database for WordPress */
  define('DB_NAME', 'tharris_wpdb');

  /** MySQL database username */
  define('DB_USER', 'root');

  /** MySQL database password */
  define('DB_PASSWORD', 'root');

  /** MySQL hostname */
  define('DB_HOST', 'localhost');

  /** Database Charset to use in creating database tables. */
  define('DB_CHARSET', 'utf8mb4');

  /** The Database Collate type. Don't change this if in doubt. */
  define('DB_COLLATE', '');

  define('WP_HOME', 'http://localhost:8888/tharrisconsultants.com'); 
  define('WP_SITEURL', 'http://localhost:8888/tharrisconsultants.com');
} else {
  // ** MySQL settings - You can get this info from your web host ** //
  /** The name of the database for WordPress */
  define('DB_NAME', 'adamsaid_wp462');

  /** MySQL database username */
  define('DB_USER', 'adamsaid_wp462');

  /** MySQL database password */
  define('DB_PASSWORD', '8W.tcS0p.8');

  /** MySQL hostname */
  define('DB_HOST', 'localhost');

  /** Database Charset to use in creating database tables. */
  define('DB_CHARSET', 'utf8mb4');

  /** The Database Collate type. Don't change this if in doubt. */
  define('DB_COLLATE', '');
}
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'adzts2m6ht8sptvpptjochojroh7gwjqa8o7ux4wga985utjglabetb9ubqsod22');
define('SECURE_AUTH_KEY',  '8zk7wxaes9lj2a1ofnhzgn8w3buapjn3hxxsqf4i6t7hqecgqloe2a3pqmipl0hl');
define('LOGGED_IN_KEY',    'sf18arjefwaddorvczheml54ytfrrcrhby9hgwumail8y28dvzkj93fb8yoc36tm');
define('NONCE_KEY',        'royvjpxgv9p784jiguyk9j2mcemj6daeo1hpjszadg9bk13pfn6m6bkmsjbwhjup');
define('AUTH_SALT',        'elmz1zvpb1zv90owqg87cedpmc4ljcshonf7urwu8l7khto9gfztysjw5upusp8i');
define('SECURE_AUTH_SALT', 'fshdc8tbyzpxuxnevrn9aduwhblyzxgxujf6hfsuzhdzihmpfoxxipqulcqg8liv');
define('LOGGED_IN_SALT',   '7heo5lggwq9ubijoduay7w4d17kaqys3wise8hmwhjwbdsxi5re1ypsm0nn6rz9k');
define('NONCE_SALT',       'mqtijnubhnup7zk9c0uwqdrbeyemu4khsjyeluf6ctf9sbpqcnrg2xt8dctxzlhl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wprm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT', '128M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

# Disables all core updates. Added by SiteGround Autoupdate:
define( 'WP_AUTO_UPDATE_CORE', false );
