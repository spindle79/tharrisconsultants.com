<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';

  $wp_customize->add_section( 'logo_section' , array(
      'title'       => __( 'Logos', 'logo_section' ),
      'priority'    => 30,
      'description' => 'Upload a logo for this theme',
  ) );

  $wp_customize->add_setting( 'header_large_logo' );
  $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'header_large_logo', array(
      'label'    => __( 'Header Large Logo', 'header_large_logo' ),
      'section'  => 'logo_section',
      'settings' => 'header_large_logo',
  ) ) );  

  $wp_customize->add_setting( 'header_small_logo' );
  $wp_customize->add_control( new \WP_Customize_Image_Control( $wp_customize, 'header_gradient_small_logo', array(
      'label'    => __( 'Header Small Logo', 'header_small_logo' ),
      'section'  => 'logo_section',
      'settings' => 'header_small_logo',
  ) ) );  

}

add_action('customize_register', __NAMESPACE__ . '\\customize_register');


/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');