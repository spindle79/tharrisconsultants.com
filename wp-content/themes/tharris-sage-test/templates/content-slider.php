
<?php if( have_rows('slider') ): ?>
  <section class='carousel-outer-wrapper'>
    <div class='container-fluid no-padding'>
      <div class='carousel-inner-wrapper gradient-outer'>
        <div class='gradient-inner-top gradient-inner-bottom'>
          <div class='row'>
            <div class='col col-xs-12'>
              <div class="carousel slider">
                <?php while( have_rows('slider') ): the_row(); ?>
                  <div class="slider-item ">
                    <?php 
                      $image = get_sub_field('image');
                      $header = get_sub_field('header');
                      $subHeader = get_sub_field('sub_header');
                      $ctaText = get_sub_field('cta_text');
                      $ctaUrl = get_sub_field('cta_url');

                      if( !empty($image) ):
                    ?>

                      <div class="image" style="background-image:url(http:<?= $image['url']; ?>);" >
                        <div class="overlay">
                          <div class="col col-xs-10 container x-padding row-eq-height align-center">
                            <div class="description">
                              <?php 
                                if( !empty($header) ){ echo("<h2 class='title'>" . $header . "</h2>" ); }
                                
                                if( !empty($subHeader) ){ echo("<p class='sub-title'>" . $subHeader . "</p>" ); }
                                
                                if( !empty($ctaText) && !empty($ctaUrl) ):
                              ?>
                                  <a href='<?= $ctaUrl ?>' class='btn btn-primary'><?= $ctaText ?></a>
                              <?php 
                                endif;
                              ?>
                            </div>
                          </div>
                        </div> <!-- /.image-overlay -->
                      </div>
                    <?php 
                      endif;
                    ?>
                  </div>
                <?php endwhile; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>