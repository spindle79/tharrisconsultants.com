<?php
  while ( have_rows('teaser') ) : the_row();
    $header = get_sub_field('teaser_header');
    $copy = get_sub_field('teaser_sub_header');
    $ctaText = get_sub_field('teaser_cta_text');
  endwhile;

  $logo = get_field('logo');
  $website = get_field('partner_website');
  
  if(empty($ctaText)){
    $ctaText = "View Website";
  }
?>

<div class='grid-item col-xs-12 col-sm-6 col-lg-6 teaser'>
  <div class='teaser-outer partner'>
    <div class='frame' style='background-image:url(http:<?= $logo['url']; ?>);'>
      <div class='teaser-inner'>
        <h3 class='header-copy'><a href='<?= $website ?>'><?= $header ?></a></h3>
        <p class='copy category'><?= $copy ?></p>
      </div>
    </div>
    <div class='teaser-link-wrapper'>
      <a href='<?= $website ?>' class='teaser-link'><?= $ctaText ?></a>
    </div>
  </div>
</div>