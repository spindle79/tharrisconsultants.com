<?php 
  while ( have_rows('section_content') ) : the_row();
    $teaserType = get_sub_field('teaser_type');
    $teasers = get_sub_field('teaser_items');
    $template = get_row_layout();
  endwhile;

  if(!empty($teaserType)){
    $teasers = get_posts(array(
      'posts_per_page'  => 100,
      'post_type'     => $teaserType,
      'post_parent' => 0
    ));
  }

  if( $teasers ):
?>
    <section>
      <div class='container'>
        <div class='gradient-outer-gold'>
          <div class='html-section-wrapper'>
            <div class='row'>
              <div class='col-xs-12 col-sm-12'>
                <div class='frame'>
                  <div class='html-section'>
                    <?php
                      get_template_part('templates/content', 'section-heading');
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
          <div class='teasers-outer-wrapper d-flex align-items-center'>
            <div class='teasers-inner-wrapper'>
              <div class='grid row teaser-items'>
                <?php
                  foreach( $teasers as $post ):
                    setup_postdata( $post );
                    $postType = get_post_type();
                    
                    
                    if($postType == "clients"){
                      get_template_part('templates/content', 'teaser-list-item');
                    }elseif($postType == "services"){
                      get_template_part('templates/content', 'teaser-list-item-services');
                    }elseif($postType == "partners"){
                      get_template_part('templates/content', 'teaser-list-item-partners');
                    }else{
                      echo "nope";
                    }
                  endforeach;
                  wp_reset_postdata();
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php 
  endif;
?>
