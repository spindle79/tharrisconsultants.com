single
<?php
  while (have_posts()) : the_post();
    get_template_part('templates/content', 'sections');
    $relatedClients = get_field('clients');
  endwhile;

  

//  if(!empty($teaserType)){
    $relatedServices = get_posts(array(
      'posts_per_page'  => 100,
      'post_type'     => get_post_type(),
      'post_parent__in' => array( $post->ID, wp_get_post_parent_id($post->ID)),
      'post__not_in' => array( $post->ID )
    ));
  //}

  if( $relatedServices ):
?>
    <section>
      <div class='container'>
        <div class=''>
          <div class='section-header'>
            <h2 class='header-copy'>Related Services</h2>
          </div>
        
          <div class='teasers-outer-wrapper d-flex align-items-center'>
            <div class='teasers-inner-wrapper'>
              <div class='grid row teaser-items'>
                <?php
                  foreach( $relatedServices as $post ):
                    setup_postdata( $post );
                    $postType = get_post_type();
                    
                    if($postType == "clients"){
                      get_template_part('templates/content', 'teaser-list-item');
                    }elseif($postType == "services"){
                      get_template_part('templates/content', 'teaser-list-item-services');
                    }elseif($postType == "partners"){
                      get_template_part('templates/content', 'teaser-list-item-partners');
                    }else{
                      echo "nope";
                    }
                  endforeach;
                  wp_reset_postdata();
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php 
  endif;

if( $relatedClients ):
?>
    <section>
      <div class='container'>
        <div class=''>
          <div class='section-header'>
            <h2 class='header-copy'>Featured Clients</h2>
          </div>
        
          <div class='teasers-outer-wrapper d-flex align-items-center'>
            <div class='teasers-inner-wrapper'>
              <div class='grid row teaser-items'>
                <?php
                  foreach( $relatedClients as $post ):
                    setup_postdata( $post );
                    $postType = get_post_type();
                    
                    if($postType == "clients"){
                      get_template_part('templates/content', 'teaser-list-item');
                    }elseif($postType == "services"){
                      get_template_part('templates/content', 'teaser-list-item-services');
                    }elseif($postType == "partners"){
                      get_template_part('templates/content', 'teaser-list-item-partners');
                    }else{
                      echo "nope";
                    }
                  endforeach;
                  wp_reset_postdata();
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php 
  endif;
?>