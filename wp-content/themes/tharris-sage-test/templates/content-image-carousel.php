<section class='carousel-outer-wrapper '>
  <div class=''>
    <div class='carousel-inner-wrapper'>
      <?php
        get_template_part('templates/content', 'section-heading');
      
        while ( have_rows('section_content') ) : the_row();
          $sliderClass = get_sub_field('css_class');
          if( empty($sliderClass) ){
            $sliderClass = "default-slider";
          }

      ?>
          <div class='gradient-inner-top gradient-inner-bottom <?= $sliderClass ?>'>
            <div class='row'>
              <div class='col-xs-12 col-sm-12'>
                <div class="carousel slider">
                
                <?php     
                  while ( have_rows('carousel_slides') ) : the_row();
                ?>
                    <div class="slider-item">
                      <?php 
                        $image = get_sub_field('image');
                        $header = get_sub_field('header');
                        $subHeader = get_sub_field('sub_header');
                        $ctaText = get_sub_field('cta_text');
                        $ctaUrl = get_sub_field('cta_url');

                        if( !empty($image) ):
                      ?>

                        <div class="image" style="background-image:url(http:<?= $image['url']; ?>);" >
                          <div class="overlay">
                            <div class="">
                              <div class="description">
                                <?php 
                                  if( !empty($header) ){
                                    echo("<h2 class='title'>" . $header . "</h2>" );
                                  }
                                  
                                  if( !empty($subHeader) ){
                                    echo("<p class='sub-title'>" . $subHeader . "</p>" );
                                  }
                                  
                                  if( !empty($ctaText) && !empty($ctaUrl) ):
                                ?>
                                    <a href='<?= $ctaUrl ?>' class='btn btn-primary'><?= $ctaText ?></a>
                                <?php 
                                  endif;
                                ?>
                              </div>
                            </div>
                          </div> <!-- /.image-overlay -->
                        </div>
                      <?php 
                        endif;
                      ?>
                    </div>
                  <?php 
                  endwhile;
                ?>
              </div>
            </div>
          </div>
        </div>
      <?php 
        endwhile;
      ?>
    </div>
  </div>
</section>
