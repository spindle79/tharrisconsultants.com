<?php use Roots\Sage\Titles; ?>

<?php if(false && !is_front_page()): ?>
  <div class="page-header">
    <h1 id='page-title' class='viewable'><?= Titles\title(); ?></h1>
  </div>
<?php endif; ?>
