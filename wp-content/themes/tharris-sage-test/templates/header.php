  <header id='header' class="banner header fixed-top">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id='th-logo-header-wrapper' title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
      <img src="<?php echo get_theme_mod( 'header_small_logo' ); ?>" id="tharris-logo-header" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    </a>
  </header>
