<?php
  while ( have_rows('section_heading') ) : the_row();
    $sectionHeading = get_sub_field('heading');
    $sectionHeadingTag = get_sub_field('heading_tag');
    $sectionCtaUrl = get_sub_field('cta_url');
    $sectionIntro = get_sub_field('intro');
  endwhile;

  if(empty($sectionHeadingTag)){
    $sectionHeadingTag = "h2"; 
  }

  if($sectionHeading || $sectionIntro):
?>
    <div class='section-header'>
      <?php
        if($sectionHeading):
          if($sectionCtaUrl):
            echo "<a href='" . $sectionCtaUrl . "'>";
          endif;
          
          echo "<" . $sectionHeadingTag . " class='header-copy'>" . $sectionHeading . "</" . $sectionHeadingTag . ">";
          
          if($sectionCtaUrl):
            echo "</a>";
          endif;
        endif;
      
        if($sectionIntro):
      ?>
          <div class='copy'><?= $sectionIntro ?></div>
      <? 
        endif;
      ?>
    </div>
<? 
  endif;
?>