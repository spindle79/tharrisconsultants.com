<?php 
  while ( have_rows('section_content') ) : the_row();
    $teasers = get_sub_field('teaser_items');
  endwhile;

  if( $teasers ):
?>

  <section class='teasers-outer-wrapper d-flex align-items-center'>
    <div class='gradient-inners'>
    <div class='container'>
    <div class='teasers-inner-wrapper gradient-outer-gold'>
      <?php
        get_template_part('templates/content', 'section-heading');
      ?>
      
      <div class='row teaser-items'>
        <?php
          foreach( $teasers as $post ):
            setup_postdata( $post ); 
            $icon = get_field('icon');
            $header = get_field('teaser_header');
            $copy = get_field('teaser_sub_header');
            $ctaText = get_field('teaser_cta_text');

            if(empty($ctaText)){
              $ctaText = "Read More";
            }
        ?>

            <div class='col-xs-12 col-sm-12 col-lg-6 col-xl-6 col-xxl-4 teaser'>
              <div class='teaser-outer'>
                <div class='frame'>
                  <div class='teaser-inner'>
                    <div class='icon-wrapper'><i class='icon <?= $icon ?>'></i></div>
                    <h3 class='header-copy'><a href='<?= get_permalink() ?>'><?= $header ?></a></h3>
                    <p class='copy'><?= $copy ?></p>
                    
                  </div>
                </div>
                <div class='teaser-link-wrapper'>
                  <a href='<?= get_permalink() ?>' class='teaser-link'><?= $ctaText ?></a>
                </div>
              </div>
            </div>
        
        <?php
          endforeach;
          wp_reset_postdata();
        ?>
      </div>
    </div>
    </div>
  </section>
<?php 
  endif;
?>