single
<?php
  while (have_posts()) : the_post();
    //get_template_part('templates/page', 'header');
    get_template_part('templates/content', 'sections');
  endwhile;

//  if(!empty($teaserType)){
    $teasers = get_posts(array(
      'posts_per_page'  => 100,
      'post_type'     => get_post_type(),
      'post_parent' => $post->ID
    ));
  //}

  if( $teasers ):
?>
    <section>
      <div class='container'>
        <div class='gradient-outer-gold'>
          <div class='section-header'>
            <h2 class='header-copy'>Related <?= get_the_title() ?> Services</h2>
          </div>
        
          <div class='teasers-outer-wrapper d-flex align-items-center'>
            <div class='teasers-inner-wrapper'>
              <div class='grid row teaser-items'>
                <?php
                  foreach( $teasers as $post ):
                    setup_postdata( $post );
                    $postType = get_post_type();
                    
                    
                    if($postType == "clients"){
                      get_template_part('templates/content', 'teaser-list-item');
                    }elseif($postType == "services"){
                      get_template_part('templates/content', 'teaser-list-item-services');
                    }elseif($postType == "partners"){
                      get_template_part('templates/content', 'teaser-list-item-partners');
                    }else{
                      echo "nope";
                    }
                  endforeach;
                  wp_reset_postdata();
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php 
  endif;
?>