<?php
  $icon = get_field('icon');
  $header = get_field('teaser_header');
  $copy = get_field('teaser_sub_header');
  $ctaText = get_field('teaser_cta_text');

  if(empty($ctaText)){
    $ctaText = "Read More";
  }
?>

<div class='grid-item col-xs-12 col-sm-12 col-lg-6 teaser'>
  <div class='teaser-outer service'>
    <div class='frame'>
      <div class='teaser-inner'>
        <div class='icon-wrapper'><i class='icon <?= $icon ?>'></i></div>
        <h3 class='header-copy'><a href='<?= get_permalink() ?>'><?= $header ?></a></h3>
        <p class='copy'><?= $copy ?></p>
        
      </div>
    </div>
    <div class='teaser-link-wrapper'>
      <a href='<?= get_permalink() ?>' class='teaser-link'><?= $ctaText ?></a>
    </div>
  </div>
</div>