<?php
  $post = get_post(28713);
  setup_postdata( $post );
  get_template_part('templates/content', 'sections');
  wp_reset_postdata();
?>

<footer class="footer">
  <div class="bottom-bar">
    <div class="center">                                   
      <small class="copyright text-center">Copyright &copy; <?= date("Y") ?> <a href="/">THarris Consultants</a></small>                 
    </div><!--//container-->
  </div><!--//bottom-bar-->
</footer>

<div id='footer-nav-wrapper'>
  <?php
    wp_nav_menu( array(
      'menu'              => 'footer',
      'theme_location'    => 'footer',
      'depth'             => 2,
      'container'         => '',
      'container_class'   => '',
      'container_id'      => 'footer-nav',
      'menu_class'        => '',
      'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
      'walker'            => new wp_bootstrap_navwalker())
    );
  ?>
</div>

<?php wp_footer(); ?>