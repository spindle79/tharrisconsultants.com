
<?php 
  if( have_rows('sections') ):
    while ( have_rows('sections') ) : $section = the_row();
      while ( have_rows('section_content') ) : the_row();
        $template = get_row_layout();
      endwhile;

      if( $template == 'testimonials' ): 
        get_template_part('templates/content', 'testimonials');

      elseif( $template == 'contact_form' ): 
        get_template_part('templates/content', 'contact-form');

      elseif( $template == 'carousel' ): 
        get_template_part('templates/content', 'image-carousel');

      elseif( $template == 'teasers' || $template == 'teaser_list_manual' || $template == "teaser_list_type" ): 
        get_template_part('templates/content', 'teaser-list');

      elseif( $template == 'html_section' ): 
        get_template_part('templates/content', 'html-section');
        
      else:
        get_template_part('templates/content', 'section-heading');

      endif;
    endwhile;
  endif;
?>