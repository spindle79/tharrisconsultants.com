
<section class='html-section-wrapper align-items-center'>
  <?php
    while ( have_rows('section_content') ) : the_row();
      $htmlSectionCopy = get_sub_field('html_copy');
      $cssClass = get_sub_field('css_class');
    endwhile;

    if(!empty($htmlSectionCopy)):
  ?>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 col-sm-12'>
            <div class='frame gradient-outer-gold'>
              <div class='html-section'>
                <?php get_template_part('templates/content', 'section-heading'); ?>
                <div class='copy'><?= $htmlSectionCopy ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
  <?php
    endif;
  ?>
</section>