<?php 
  while ( have_rows('section_content') ) : the_row();
    $teasers = get_sub_field('teaser_items');
  endwhile;

  if( $teasers ):
?>

  <section class='teasers-outer-wrapper d-flex align-items-center'>
    <div class='gradient-inners'>
      <div class='container'>
        <div class='teasers-inner-wrapper gradient-outer-gold'>
        <?php
          get_template_part('templates/content', 'section-heading');
        ?>
        
          <div class='grid row teaser-items'>
            <?php
              foreach( $teasers as $post ):
                setup_postdata( $post );
                $postType = get_post_type();
                echo $postType;

                if($postType == "client"){

                }
                $logo = get_field('logo');
                $screenshot = get_field('website_screenshot');
                $header = get_field('client_name');
                $copy = get_field('client_website');

                if(!empty($screenshot)){
                  $ctaText = "View Screenshot";
                }
            ?>

              <div class='grid-item col-xs-12 col-sm-6 col-lg-6 col-xl-4 col-xxl-4 teaser'>
                <div class='teaser-outer client'>
                  <div class='frame'  style='background-image:url(http:<?= $logo['url']; ?>);'>
                    <div class='teaser-inner'>
                      <h3 class='header-copy'><a href='<?= get_permalink() ?>'><?= $header ?></a></h3>
                      <p class='copy'><?= $copy ?></p>
                    </div>
                  </div>
                  <div class='teaser-link-wrapper'>
                    <a href='<?= get_permalink() ?>' class='teaser-link'>Read More</a>
                  </div>
                </div>
              </div>
            
            <?php
              endforeach;
              wp_reset_postdata();
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php 
  endif;
?>

<style>
.client .frame{
  background-position: center;
  background-size: 50%;
  background-repeat: no-repeat;
}
.teaser .client .teaser-inner{
  opacity:0;
  padding-top: 10vh !Important;
  padding-bottom: 10vh !Important;
  transition:.25s ease-in;
}
.teaser:hover .client .teaser-inner{
  opacity:1;
  background-color: hsla(0,0%,100%,.99);
}

.client .frame{
  
}
</style>