<?php 
  while ( have_rows('section_content') ) : the_row();
    $testimonials = get_sub_field('testimonial_items');
    $sliderClass = get_sub_field('css_class');
    if( empty($sliderClass) ){
      $sliderClass = "default-slider";
    }
  endwhile;

  if( $testimonials ):
?>
  <section class='testimonials-outer-wrapper <?= $sliderClass ?>'>
    <div class=''>
      <div class='testimonials-inner-wrapper gradient-inner'>
        <?php
          //get_template_part('templates/content', 'section-heading');
        ?>
        
      
        <div class="carousel slider">
          <?php
            foreach( $testimonials as $testimonial ):
              $post = $testimonial;
              setup_postdata( $post ); 
              
              $person = get_field('person');
              $personUrl = get_field('person_linkedin_profile_url');
              $position = get_field('position');
              $bgLogo = get_field('background_image_override');
              $logo = get_field('logo_image_override');
              
              if(get_field('short_copy')){
                $copy = "<p>" . get_field('short_copy') . "</p>";
              }else{
                $copy = get_field('copy');
              }
              
              if(get_field('is_current_client')){
                $client = get_field('client');
                $post = $client;
                setup_postdata( $client ); 
                
                if(!$logo){
                  $logo = get_field('logo');
                }
                
                if(!$bgLogo){
                  $bgLogo = get_field('logo');
                }
                $clientName = get_field('client_name');
                $clientWebsite = get_field('client_website');

                wp_reset_postdata();
              }else{
                $post = $testimonial;
                setup_postdata( $testimonial ); 
                while ( have_rows('company_information') ) : the_row();
                  if(!$logo){
                    $logo = get_sub_field('company_logo');
                  }
                  
                  if(!$bgLogo){
                    $bgLogo = get_sub_field('company_logo');
                  }

                  $clientName = get_sub_field('company_name');
                  $clientWebsite = get_sub_field('company_website');
                endwhile;
              }
                
          ?>
                <div class="slider-item" style='position: relative;'>
                  <div class="image-overlay" style="background-image:url(http:<?= $bgLogo['url']; ?>);" ></div>
                  
                    <div class="text-center">
                      <div class="description testimonial gradient-outer-gold-dense">
                        <?php
                          if($logo):
                            echo "<div class='logo-wrapper'><img src='http:" . $logo['url'] . "'/></div>";
                          endif;

                          if($copy):
                            echo "<div class='copy'>" . $copy  . "</div>";
                          endif;

                          if($person):
                            echo "<p class='person'>" . $person  . "</p>";
                          endif;
                      
                          if($position || $clientName):
                            echo "<p class='position-wrapper'>";
                          
                            echo "<span class='position'>" . $position  . "</span>";

                            if($position && $clientName):
                              echo " - ";
                            endif;
                              
                              
                            if($clientName):
                              echo "<span class='company-name'>";
                              
                              if($clientWebsite):
                                echo "<a href='" . $clientWebsite . "'>" .$clientName . "</a>";
                              else:
                                echo $clientName;
                              endif;

                              echo "</span>";
                            endif;

                            echo "</p>";
                          endif;
                        ?>
                      </div>
                    </div>
                  
                </div>
          <?php    
            endforeach;
            wp_reset_postdata();
          ?>
        </div>
      </div>
    </div>
  </section>
<?php 
  endif;
?>