<?php 
  $form = get_sub_field('form');

  if( $form ):
    $post = $form;
?>

  <section class='teasers-outer-wrapper d-flex align-items-center'>
    <div class='teasers-inner-wrapper container gradient-outer'>
      <?php
        $sectionHeader = get_sub_field('header');
        $sectionUrl = get_sub_field('header');
        $sectionIntroCopy = get_sub_field('intro_copy');
        if($sectionHeader):
      ?>
        
        <div class='section-header'>
          <h2 class='header-copy'><?= $sectionHeader ?></h2>
          <div class='intro-copy'><?= $sectionIntroCopy ?></div>
        </div>
      <? endif ?>

      <div class='row teaser-items'>
        <?php
          /*
          *  get all custom fields and dump for testing
          */

          $fields = get_fields();
          var_dump( $fields ); 

          /*
          *  get all custom fields, loop through them and load the field object to create a label => value markup
          */

          $fields = get_fields();

          if( $fields )
          {
            foreach( $fields as $field_name => $value )
            {
              // get_field_object( $field_name, $post_id, $options )
              // - $value has already been loaded for us, no point to load it again in the get_field_object function
              $field = get_field_object($field_name, false, array('load_value' => false));

              echo '<div>';
                echo '<h3>' . $field['label'] . '</h3>';
                echo $value;
              echo '</div>';
            }
          }

          wp_reset_postdata();
        ?>
      </div>
    </div>
  </section>
<?php 
  endif;
?>