<?php
  $logo = get_field('logo');
  $screenshot = get_field('website_screenshot');
  $header = get_field('client_name');
  $copy = get_field('client_website');

  if(!empty($screenshot)){
    $ctaText = "View Screenshot";
  }
?>

<div class='grid-item col col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-4 col-xxl-4 teaser'>
  <div class='teaser-outer client'>
    <div class='frame' style='background-image:url(http:<?= $logo['url']; ?>);'>
      <div class='teaser-inner'>
        <h3 class='header-copy'><a href='<?= get_permalink() ?>'><?= $header ?></a></h3>
        <p class='copy'><?= $copy ?></p>
      </div>
    </div>
    <div class='teaser-link-wrapper'>
      <a href='<?= get_permalink() ?>' class='teaser-link'>Read More</a>
    </div>
  </div>
</div>