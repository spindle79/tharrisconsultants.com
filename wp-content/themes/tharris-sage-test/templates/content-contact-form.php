<?php 
  while ( have_rows('section_content') ) : the_row();
    $formId = get_sub_field('contact_form');
  endwhile;

  if( $formId ):
?>
    <section class='form-outer-wrapper'>
      <div class='gradient-inners'>
      <div class='container'>
        <div class='form-inner-wrapper gradient-outer-gold'>
          <?php
            get_template_part('templates/content', 'section-heading');
          ?>

          <div class='contact-form'>
            <?php echo do_shortcode( '[contact-form-7 id="' . $formId .'" title="Contact form 1"]' ); ?>
          </div>
        </div>
      </div>
      </div>
    </section>
<?php 
  endif;
?>