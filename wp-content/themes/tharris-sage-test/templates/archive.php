<?php while (have_posts()) : the_post(); ?>
  archive <article <?php post_class(); ?>>
    <header class="section-header">
      <div class="container">
        <h1 class="header-copy"><?php the_title(); ?> - <?php post_type_archive(); ?></h1>
      </div>
    </header>
    
    <div class="entry-content container">
      <?php the_content(); ?>
    </div>
    <footer class="container">
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>

<?php endwhile; ?>
