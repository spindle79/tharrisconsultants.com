<?php if(is_front_page()): ?>
  <section id="title-promo" class="title-promo">
    <?php if ( get_theme_mod( 'header_large_logo' ) ) : ?>
      <img class='tharris-logo' src="<?php echo get_theme_mod( 'header_large_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    <?php endif; ?>
  </section>
<?php endif; ?>


<?php
  get_template_part('templates/page', 'header');
  while (have_posts()) : the_post();
    get_template_part('templates/content', 'sections');
  endwhile;
?>