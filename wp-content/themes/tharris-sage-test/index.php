<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php if( have_rows('slider') ): ?>
 
 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="..." alt="First slide">
    </div>
     <?php while( have_rows('slider') ): the_row(); ?>
        <div class="carousel-item">
          <?php 
            $image = get_field('image');
            $header = get_field('header');
            $subHeader = get_field('sub_header');
            $ctaText = get_field('cta_text');
            $ctaUrl = get_field('cta_url');

            if( !empty($image) ):
          ?>
              <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" />
          <?php 
            endif;
          ?>
          
          <div class="carousel-caption d-none d-md-block">
            <?php 
              if( !empty($header) ){ echo("<h2>" . the_sub_field('header') . "</h2>" ); }
              
              if( !empty($subHeader) ){ echo("<p>" . the_sub_field('subHeader') . "</p>" ); }
              
              if( !empty($ctaText) && !empty($ctaUrl) ):
            ?>
                <a href='<?= $ctaUrl ?>' class='button'><?= $ctaText ?></a>
            <?php 
              endif;
            ?>
          </div>
        </div>
    <?php endwhile; ?>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

 
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?>
