/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        /* ======= Twitter Bootstrap hover dropdown ======= */   
        /* Ref: https://github.com/CWSpear/bootstrap-hover-dropdown */ 
        /* apply dropdownHover to all elements with the data-hover="dropdown" attribute */
        
        //$('[data-hover="dropdown"]').dropdownHover();
        
        /* ======= jQuery Responsive equal heights plugin ======= */
        /* Ref: https://github.com/liabru/jquery-match-height */
        
         /*
         $('#who .item-inner').matchHeight();    
         $('#testimonials .item-inner .quote').matchHeight(); 
         $('#latest-blog .item-inner').matchHeight(); 
         $('#services .item-inner').matchHeight();
         $('#team .item-inner').matchHeight();
         */
         
        /* ======= jQuery Placeholder ======= */
        /* Ref: https://github.com/mathiasbynens/jquery-placeholder */
        
        //$('input, textarea').placeholder();         
        
        /* ======= jQuery FitVids - Responsive Video ======= */
        /* Ref: https://github.com/davatron5000/FitVids.js/blob/master/README.md */    
        //$(".video-container").fitVids();   
        
      
      /* ======= Fixed Header animation ======= */ 
            
        var $footer = $('footer');
        var $footerNav = $('#footer-nav-wrapper');
        var $header = $('#header');
        var $headerTharrisLogo = $('#tharris-logo-header');
        var $pageTitle = $('#page-title');
          
        $(window).on('scroll', function() {
          var scrollTop = $(window).scrollTop();
          var windowHeight = $(window).height();
          var documentHeight = $(document).height();

          if (scrollTop > windowHeight/3 ) {
            $header.addClass('header-shrink');
            $headerTharrisLogo.addClass('viewable');
            //$pageTitle.removeClass('viewable');        
          }else{
            $headerTharrisLogo.removeClass('viewable');
            //$pageTitle.addClass('viewable');
            $header.removeClass('header-shrink');
          }

          var footerHeight = $('footer').height();
          var footerShownHeight = documentHeight - scrollTop - windowHeight;

          //console.log(footerShownHeight, footerHeight)
          
          if(footerShownHeight <= footerHeight) {
            $footerNav.css({'bottom':footerHeight - footerShownHeight});
          }else if($footerNav.css('bottom') !== 0){
            $footerNav.css({'bottom':'0px'});
          }
        });

        $('.homepage-slider .slider').slick({
          slidesToShow: 1,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 5000,
          speed: 1000,
          fade: true,
          cssEase: 'ease-in-out',
          pauseOnHover:false,
          infinite: true
        });

        $('.default-slider .slider').slick({
          slidesToShow: 1,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 2500
        });

        try{Typekit.load();}catch(e){}
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
